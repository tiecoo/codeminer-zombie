class Addcolumntobag < ActiveRecord::Migration[5.0]
  def change
      add_column  :bags, :quantity, :integer
      add_column :bags, :survivor_id , :integer, :references => 'survivor'
      add_column :bags, :items_id , :integer, :references => 'items'
  end
end
