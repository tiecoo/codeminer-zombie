class CreateTransfers < ActiveRecord::Migration[5.0]
  def change
    create_table :transfers do |t|
      t.integer :survivor_in
      t.integer :quantity_in
      t.integer :item_in
      t.integer :survivor_out
      t.integer :quantity_out
      t.integer :item_out

      t.timestamps
    end
  end
end
