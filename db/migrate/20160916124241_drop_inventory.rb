class DropInventory < ActiveRecord::Migration[5.0]
  def down
    drop_table :inventories
  end

def up
    create_table :bags do |t|
      t.timestamps
    end
  end
end
