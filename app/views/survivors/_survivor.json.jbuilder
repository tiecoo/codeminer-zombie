json.extract! survivor, :id, :name, :gender, :longitude, :latitude, :age, :infected, :created_at, :updated_at
json.url survivor_url(survivor, format: :json)