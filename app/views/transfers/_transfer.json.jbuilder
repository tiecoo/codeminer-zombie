json.extract! transfer, :id, :survivor_in, :quantity_in, :item_in, :survivor_out, :quantity_out, :item_out, :created_at, :updated_at
json.url transfer_url(transfer, format: :json)