class Survivor < ApplicationRecord
   has_many :bags
   accepts_nested_attributes_for :bags, reject_if: :all_blank, allow_destroy: true
end
