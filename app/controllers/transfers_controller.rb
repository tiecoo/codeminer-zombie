class TransfersController < ApplicationController
  before_action :set_transfer, only: [:show, :edit, :update, :destroy]

  # GET /transfers
  # GET /transfers.json
  def index
    @transfers = Transfer.all
  end

  # GET /transfers/1
  # GET /transfers/1.json
  def show
  end

  # GET /transfers/new
  def new
    @transfer = Transfer.new
  end

  # GET /transfers/1/edit
  def edit
  end

  # POST /transfers
  # POST /transfers.json
  def create
    @transfer = Transfer.new(transfer_params)
    bagin =  Bag.where(survivor_id: @transfer.survivor_in)
    bagin.each do |bag|
        if bag.item_ref == @transfer.item_in
          if bag.quantity <= @transfer.quantity_in
            @points = Item.where(id: @transfer.item_in)
            @points_in = @transfer.quantity_in * @points.points
          else
            @notice =  'Transfer couldnt happen cause Quantity of items unavaible from the first survivor'
            return new_transfer_path
          end
        else
          @notice= 'Transfer couldnt happen cause items unavaible from the first survivor'
          return new_transfer_path
        end
    end
    bagout =  Bag.where(survivor_id: @transfer.survivor_out)
    bagout.each do |bag|
        if bag.item_ref == @transfer.item_out
          if bag.quantity <= @transfer.quantity_out
            @points = Item.where(id: @transfer.item_out)
            @points_out = @transfer.quantity_out * @points.points
          else
            @notice =  'Transfer couldnt happen cause Quatity of items unavaible from the second survivor'
             return new_transfer_path
          end
        else
          @notice =  'Transfer couldnt happen cause items unavaible from the second survivor'
             return new_transfer_path
      end
    end

    if @points_in == @points_out
      @bagin = Bag.where(survivor_id: @transfer.survivor_in).where(item_id: @transfer.item_in)
      @bagout = Bag.where(survivor_id: @transfer.survivor_out).where(item_id: @transfer.item_out)
      @bagin.survivor_id = @transfer.survivor_out
      @bagout.survivor_id = @transfer.survivor_in
      @bagin.save
      @bagout.save
      @transfer.save
        redirect_to @transfer, notice: 'Transfer was successfully created. Points changed'
    else
      @notice =  'Transfer couldnt happen'
             return new_transfer_path
    end

  end

  # PATCH/PUT /transfers/1
  # PATCH/PUT /transfers/1.json
  def update
    respond_to do |format|
      if @transfer.update(transfer_params)
        format.html { redirect_to @transfer, notice: 'Transfer was successfully updated.' }
        format.json { render :show, status: :ok, location: @transfer }
      else
        format.html { render :edit }
        format.json { render json: @transfer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transfers/1
  # DELETE /transfers/1.json
  def destroy
    @transfer.destroy
    respond_to do |format|
      format.html { redirect_to transfers_url, notice: 'Transfer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transfer
      @transfer = Transfer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transfer_params
      params.require(:transfer).permit(:survivor_in, :quantity_in, :item_in, :survivor_out, :quantity_out, :item_out)
    end
end