class API::V1::ItemsController < ApplicationController

  def index
    @items = Item.all
        render :json => @items.map { |item| {:id => item.id, :name => item.name, :points => item.points} }
  end

end