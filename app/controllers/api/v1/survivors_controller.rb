class API::V1::SurvivorsController < ApplicationController

  def index
    @survivors = Survivor.all
        render :json => @survivors.map { |survivor| {:id => survivor.id, :name => survivor.name, :infected => survivor.infected} }
  end

def create
    @survivor = Survivor.new(survivor_params)
    if @survivor.save
      respond_with @survivor
    end
  end


    def show
  @survivor = Survivor.where(id: params[:id])
  @items = Bag.where(survivor_id: params[:id])
respond_to do |format|
  format.json  { render :json => {:survivor => @survivor.map { |survivor| {:id => survivor.id, :name => survivor.name, :infected => survivor.infected, :gender => survivor.gender, :latitude => survivor.latitude, :longitude => survivor.longitude, :age => survivor.age} },
                                  :inventory => @items.map { |items| {:id => items.id, :quantity => items.quantity, :item => items.item_ref}} }}
end
end
private
 def set_survivor
      @survivor = Survivor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def survivor_params
      params.require(:survivor).permit(:name, :gender, :longitude, :latitude, :age, :infected, bags_attributes: [:id, :item_ref, :quantity])
    end
end