Rails.application.routes.draw do
  resources :transfers
  resources :survivors
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


    namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :survivors
      resources :transfers
      resources :items
    end
  end

end
